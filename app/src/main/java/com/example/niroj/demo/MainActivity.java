package com.example.niroj.demo;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public  class MainActivity extends AppCompatActivity {




    EditText textIn,txtHeading;
    Button buttonAdd,btnsave;
    TextView textViewOut;
    LinearLayout container;

    ExpandableDataPump expandableDataPump;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);




        textViewOut = (TextView)findViewById(R.id.textout);
        textIn = (EditText)findViewById(R.id.textin);
        txtHeading = (EditText)findViewById(R.id.heading);
        buttonAdd = (Button)findViewById(R.id.add);
        container = (LinearLayout)findViewById(R.id.container);


        buttonAdd.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View arg0) {


                LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View addView = layoutInflater.inflate(R.layout.list_view, null);

                TextView textOut = (TextView)addView.findViewById(R.id.textout);

                textOut.setText(textIn.getText().toString());


                Button buttonRemove = (Button)addView.findViewById(R.id.remove);
                buttonRemove.setOnClickListener(new View.OnClickListener(){

                    @Override
                    public void onClick(View v) {
                        ((LinearLayout)addView.getParent()).removeView(addView);
                    }});

                container.addView(addView);
            }});

        btnsave =(Button) findViewById(R.id.btn_save);
        btnsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    expandableDataPump.getData();
                    Intent intent = new Intent(getApplicationContext(),ExpandableList.class);
                    /*intent.putExtra("data",txtHeading.getText().toString());
                    intent.putExtra("data1",textViewOut.getText().toString());*/
                    startActivity(intent);
                }
                catch (Exception ex) {

Toast.makeText(MainActivity.this,"You have an ERROR",Toast.LENGTH_LONG).show();                }



            }
        });

    }
    public  class ExpandableDataPump {
        public  HashMap<String, List<String>> getData() {
            HashMap<String, List<String>> expandableListDetail = new HashMap<String, List<String>>();


            List<String> childs = new ArrayList<String>();
            childs.add(textViewOut.getText().toString());
            expandableListDetail.put(txtHeading.getText().toString(), childs);

            return expandableListDetail;
        }
    }


}